import QtQuick 2.4

Rectangle {
    id: root

    width: backPanelVisible ? main_row.width : 0
    color: "#efefef"

    Row {
        id: main_row

        anchors {
            left: parent.left
            verticalCenter: parent.verticalCenter
        }

        Repeater {
            model: ["backs/background1.svg", "backs/background2.svg", "backs/background3.svg", "backs/background4.svg"]
            BackgroundThumb {
                imgLink: modelData
                width: 3 * height / 2
                height: panelHeight
            }
        }
    }

}

