import QtQuick 2.4

Rectangle {
    id: frame

    property var img_source
    property int img_width
    property int img_height

    width: img_width + units.gu(8)
    height: img_height + units.gu(8)
    x: - units.gu(4)
    y: panelHeight - units.gu(4)

    Image {
        id: back_img

        source: backgroundImg
        sourceSize.height: img_height
        anchors.centerIn: parent
        width: parent.width - units.gu(8)
        fillMode: Image.PreserveAspectCrop
    }

    Image {
        id: img

        source: img_source
        width: img_width
        height: img_height
        anchors.centerIn: parent
    }

    SequentialAnimation {
        id: snap_anim

        NumberAnimation { target: frame; property: "scale"; to: .5 }

        PauseAnimation { duration: 200 }

        ParallelAnimation {
            NumberAnimation { target: frame; property: "scale"; to: 0; duration: 300 }
            NumberAnimation { target: frame; property: "x"; to: - width * .5 + panelHeight * 1.5; duration: 200;}
            NumberAnimation { target: frame; property: "y"; to: - height * .5 - panelHeight / 2; duration: 300 }
        }

        PauseAnimation {duration: 2000}

        onRunningChanged: if (!running) { frame.destroy() }
    }

    Component.onCompleted: snap_anim.start()
}

