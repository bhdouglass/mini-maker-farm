import QtQuick 2.4

ListModel {
    ListElement {
        image: "kon"
        widthS: 22
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "byk"
        widthS: 22
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "krowa"
        widthS: 19
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "koza"
        widthS: 16
        scaleMin: .4
        scaleMax: 3
    }
    ListElement {
        image: "baran"
        widthS: 16
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "owca"
        widthS: 15
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "osiol"
        widthS: 12
        scaleMin: .2
        scaleMax: 3.2
    }
    ListElement {
        image: "swinia"
        widthS: 12
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "ges"
        widthS: 11
        scaleMin: .4
        scaleMax: 3.2
    }
    ListElement {
        image: "kogut"
        widthS: 12
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "kura"
        widthS: 11
        scaleMin: .6
        scaleMax: 4
    }
    ListElement {
        image: "kaczka"
        widthS: 10
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "indyk"
        widthS: 12
        scaleMin: .2
        scaleMax: 4
    }
    ListElement {
        image: "pies"
        widthS: 12
        scaleMin: .2
        scaleMax: 3
    }
    ListElement {
        image: "pies2"
        widthS: 11
        scaleMin: .2
        scaleMax: 5
    }
    ListElement {
        image: "kot"
        widthS: 8
        scaleMin: .2
        scaleMax: 5
    }
    ListElement {
        image: "krolik"
        widthS: 8
        scaleMin: .2
        scaleMax: 5
    }
}

