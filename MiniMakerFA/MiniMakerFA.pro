TEMPLATE = aux
TARGET = MiniMakerFA

RESOURCES += MiniMakerFA.qrc

QML_FILES += $$files(*.qml,true) \
             $$files(*.js,true)

CONF_FILES +=  MiniMakerFA.apparmor \
               MiniMakerFA.png \
               Splash.png

OTHER_FILES += $${CONF_FILES} \
               $${QML_FILES} \
               MiniMakerFA.desktop

#specify where the qml/js files are installed to
qml_files.path = /MiniMakerFA
qml_files.files += $${QML_FILES}

#specify where the config files are installed to
config_files.path = /MiniMakerFA
config_files.files += $${CONF_FILES}

#install the desktop file, a translated version is
#automatically created in the build directory
desktop_file.path = /MiniMakerFA
desktop_file.files = $$OUT_PWD/MiniMakerFA.desktop
desktop_file.CONFIG += no_check_exist

#images files
my_images.path = /MiniMakerFA
my_images.files += images/*

#sound files
my_sounds.path = /MiniMakerFA
my_sounds.files += sounds/*

INSTALLS+=config_files qml_files desktop_file my_images my_sounds

DISTFILES += \
    BackgroundPanel.qml \
    ElementsPanel.qml \
    BackgroundThumb.qml \
    Element.qml \
    ElementBase.qml \
    ElementsModel.qml \
    StartScreen.qml \
    PlayScreen.qml \
    MenuPanel.qml \
    PressButton.qml \
    SnapImage.qml \
    PlayField.qml \
    BinPanel.qml \
    Storage.js \
    ShowScreen.qml \
    ShowSnapDelegate.qml \
    ShowElement.qml \
    DeleteConfirm.qml \
    AboutPage.qml

