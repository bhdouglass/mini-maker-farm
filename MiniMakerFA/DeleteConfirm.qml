import QtQuick 2.4

Rectangle {
    id: root

    property alias del_btn: delete_btn
    signal cancel

    color: "#90363636"

    MouseArea {
        anchors.fill: parent
        onClicked: root.cancel()
    }

    Rectangle {
        id: dialog

        color: "#efefef"
        height: 1.5 * panelHeight
        width: 3 * panelHeight
        radius: height / 8
        anchors.centerIn: parent

        PressButton {
            id: delete_btn

            width: height
            height: panelHeight
            anchors.centerIn: parent
            img_source: "ui/bin-btn.svg"
            img_press_source: "ui/bin-btn-press.svg"
            img_width: panelHeight
        }
    }

    Rectangle {
        id: close_btn

        color: "white"
        width: dialog.height / 2
        height: width
        radius: height / 2
        anchors {
            left: dialog.left
            leftMargin: - width / 2
            top: dialog.top
            topMargin: - width / 2
        }

        Rectangle {
            color: "#222"
            width: 3 * parent.width / 4
            height: parent.width / 10
            radius: height / 2
            rotation: 45
            anchors.centerIn: parent
        }

        Rectangle {
            color: "#222"
            width: 2 * parent.width / 3
            height: parent.width / 10
            radius: height / 2
            rotation: - 45
            anchors.centerIn: parent
        }
    }
}

