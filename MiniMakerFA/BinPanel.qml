import QtQuick 2.4

Item {
    id: root

    Image {
        id: bin

        source: "ui/bin.svg"
        sourceSize.height: panelHeight
        anchors.centerIn: parent
    }
    DropArea {
        id: drop_area

        anchors.fill: parent
        onEntered: inBinArea = true
        onExited: inBinArea = false
    }

}

