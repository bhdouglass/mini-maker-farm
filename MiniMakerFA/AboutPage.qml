import QtQuick 2.4
import Lomiri.Components 1.3

Rectangle {
    id: root

    color: "#efefef"

    Image {
        id: post_mm

        source: "ui/mini-maker.svg"
        anchors{
            top: parent.top
            topMargin: units.gu(4)
            horizontalCenter: parent.horizontalCenter
        }
        sourceSize.width: parent.height / 6
    }

    Label {
        id: title

        text: "Farm \n v 1.0"
        anchors {
            top: post_mm.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: info

        text: "made by Michał Prędotka and kids\nlicensed under GPL v3"
        anchors {
            top: title.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
    }

    Label {
        id: img_credit

        text: "Some images provided by <a href='http://emojione.com'>EmojiOne</a>"
        anchors {
            top: info.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: Qt.openUrlExternally(link)
    }

    Label {
        id: sound_credit

        text: "Music by <a href='http://www.blackswanmedia.co.za/'>Tyrel Parker</a> "
        anchors {
            top: img_credit.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: Qt.openUrlExternally(link)
    }

    Label {
        id: www

        text: "website: <a href='https://launchpad.net/mini-maker'>https://launchpad.net/mini-maker</a>"
        anchors {
            top: sound_credit.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: Qt.openUrlExternally(link)
    }

    Label {
        id: contact

        text: "contact: <a href='mailto:mivoligo@gmail.com'>mivoligo@gmail.com</a>"
        anchors {
            top: www.bottom
            topMargin: units.gu(2)
            horizontalCenter: parent.horizontalCenter
        }
        horizontalAlignment: Text.AlignHCenter
        onLinkActivated: Qt.openUrlExternally(link)
    }


    PressButton {
        id: home_btn

        width: height
        height: panelHeight
        img_source: "ui/home-btn.svg"
        img_press_source: "ui/home-btn-press.svg"
        img_width: panelHeight
        anchors {
            bottom: parent.bottom
            left: parent.left
        }

        onClicked: isAboutScreen = false
    }

}

