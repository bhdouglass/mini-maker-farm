import QtQuick 2.4

Item {
    id: root

    property string imgLink

    Rectangle {
        id: selected_back

        visible: imgLink == backgroundImg
        width: color_rect.width + units.gu(2)
        height: color_rect.height + units.gu(2)
        anchors.centerIn: parent
        color: "#00ffffff"
        border.color: "orange"
        border.width: units.dp(3)
        radius: 8
    }

    Rectangle {
        id: color_rect

        anchors.centerIn: parent
        width: 7 * height / 4
        height: parent.height / 2
        radius: 4

        Image {
            id: back_img

            source: imgLink
            sourceSize.height: parent.height
            anchors.centerIn: parent
            width: parent.width
            fillMode: Image.PreserveAspectCrop
        }

    }

    MouseArea {
        anchors.fill: parent
        onPressed: {
            backgroundImg = imgLink
        }
    }


}

